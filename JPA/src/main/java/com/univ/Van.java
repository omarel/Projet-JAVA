package com.univ;

import javax.persistence.Entity;

@Entity
public class Van extends Vehicule{
	
	private float maxWeight;

	public float getMaxWeight() {
		return maxWeight;
	}

	public void setMaxWeight(float maxWeight) {
		this.maxWeight = maxWeight;
	}
	

}
