package com.univ;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public abstract class Vehicule {
	@Id
	private int platenumber;
	@OneToMany (cascade=CascadeType.ALL,mappedBy = "vehicule")
	private List<Rent> Rents = new ArrayList<Rent>();
	
	
	public int getPlatenumber() {
		return platenumber;
	}

	public void setPlatenumber(int platenumber) {
		this.platenumber = platenumber;
	}
	
	public List<Rent> getRents() {
		return Rents;
	}

	public void setRents(List<Rent> rents) {
		Rents = rents;
	}
	

}
