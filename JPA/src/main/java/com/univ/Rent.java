package com.univ;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Rent {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Temporal(TemporalType.DATE)
	private Date BeginRent;
	@Temporal(TemporalType.DATE)
	private Date EndRent;
	
	@ManyToOne 
	private Person person;
	
	@ManyToOne
	private Vehicule vehicule;
	
	public Vehicule getVehicule() {
		return vehicule;
	}

	public void setVehicule(Vehicule vehicule) {
		this.vehicule = vehicule;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	

	public Date getBeginRent() {
		return BeginRent;
	}

	public void setBeginRent(Date beginRent) {
		BeginRent = beginRent;
	}
	
	public Date getEndRent() {
		return EndRent;
	}

	public void setEndRent(Date endRent) {
		EndRent = endRent;
	}
	
	
	}
	


